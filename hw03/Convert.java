

import java.util.Scanner;

public class Convert{

   	public static void main(String[] args) {
          
      Scanner myScanner = new Scanner( System.in );

      System.out.print("Enter the number of acres of land affected, in the form xx.xx: ");      //prints line asking user to input acres
      double acres = myScanner.nextDouble();        //takes in next double as number of acres
        
      
      System.out.print("Enter how many inches of rain are dropped on average, in the form xx: ");     //prints line asking user to input rain average
      double rain = myScanner.nextDouble();         //takes in next double as number of rain in inches
      
      double cubicMiles = (((acres * rain) / 12000) * 0.0003);      //converts acres and inches of rain into volume of rain per cubic miles
      
      System.out.println("The toal amount of rain in cubic miles is " + cubicMiles);      //prints amount of cubic miles of rain
                           
    }
}