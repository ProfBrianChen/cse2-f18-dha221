

import java.util.Scanner;

public class Pyramid{

   	public static void main(String[] args) {
          
      Scanner myScanner = new Scanner( System.in );

      System.out.print("Enter the height of the pyramid, in form of xx: ");     //prints line asking user to input height 
      double height = myScanner.nextDouble();       //takes next double and defines as "height"
        
      System.out.print("Enter the length of one square side of the pyramid, in the form xx: ");    //print line asking user to input side length
      double length = myScanner.nextDouble();       //takes next double and defines as "length"
      
      int lengthSquared = Math.pow(2, length);    //raises side length to power of two to find the area of base
      
      double volume = ((lengthSquared * height)/3);     //formula for finding volume of square pyramid 
      
      volume = volume *100;
      volume = (int)(volume);
      volume = volume /100;
      //these three lines stop the output from having too many decimal places
      
      System.out.println("The volume of the pyramid is " + volume);
      //outputs the final volume of pyramid
      
      
    }
}