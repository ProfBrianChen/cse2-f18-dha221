/////Doug Anderson 9/6 
///Outputs the amount of time elapsed, 

public class Cyclometer{
  
    public static void main(String args[]){
     
      int secsTrip1=480;
      int secsTrip2=3320;
      int countsTrip1=1561;
      int countsTrip2=9037;
      double wheelDiameter=27.0;
      double PI=3.14159; 
      int feetPerMile=5280;  
      int inchesPerFoot=12;   
      int secondsPerMinute=60;  
      double distanceTrip1, distanceTrip2,totalDistance;
      ///This defines all of the variables used to calculate distance traveled and time
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
      System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	//prints the time and wheel spins for trips 1 and 2
    	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    	totalDistance=distanceTrip1+distanceTrip2;
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");
      //Calculates and prints the distance traveled in miles for trips 1 and 2 
      
    }
}