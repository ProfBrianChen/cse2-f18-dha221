

import java.util.Scanner;     //import scanner

public class EncryptedX{
  
  public static void main (String arg[]){     //main method
    Scanner myScanner = new Scanner(System.in);     //declaring scanner name
    
    System.out.println("Enter an integer between 1 and 100: ");   //asks for int 1-100
    int size;       //defining the input from user
    while (! myScanner.hasNextInt()){       
      myScanner.next();
      System.out.println("Error, please enter an integer: "); 
    }   ////this all makes sure the user enters an integer
    size = myScanner.nextInt();     //declares next int as size
    
    while (size > 100) {
      System.out.println("Please input an integer between 0 and 100: ");
      size = myScanner.nextInt();     //makes sure the integer is between 1-100
    }
    if (size % 2 == 0) {    //checks if size is even 
      
      for (int i = 0; i < size; i++){       /// i starts as zero and incrememnts by 1 until it equals the
                                             // input size so it outputs that many lines
        for (int z = 0; z < size; z++ ){      // same as above except this is just for other side of square
                                              
          if (i == z || (i + 1) == size - z){   
            System.out.print(" ");              //outputs a space when the two values are equal
          }
          else {
            System.out.print("*");            //outputs square if i and z are not euqal
          }
    }
    
        System.out.println(" ");        //new line
        
      }
    if (size % 2 == 1){           //same process as above but for odd input number
      
      for (int i = 0; i < size; i++){     //i starts at zero and increments until it equals the input square size
        
        for (int z = 0; z < size; z++ ){    //same thing as above but for columns
          
          if (i == z || (i + 1) == size - z){     //if values are same prints a space
            System.out.print(" ");                //or if the side value + 1 is same as column -1 print space
          }
          else {
            System.out.print("*");        //if not that print star
            
          }
    }
        System.out.println();         //new line
        
      } 
      }
    } 
  }
}