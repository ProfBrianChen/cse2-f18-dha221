Homework 6 EncryptedX

Total Score: 80

Late        -10 pts YES

Compiles       20 pts YES

Comments    10 pts YES

Uses nested for loop properly     15 pts -5 b/c inner part of pattern is incorrect

Checks for between 0-100   5 pts SEE BELOW

Program works  (shows some sort of grid-like output)    50 pts YES

Prints x instead of *	-5 pts N/A

Asks for 1-100 instead of 0-100	-5 pts YES