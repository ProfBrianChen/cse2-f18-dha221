public class Arithmetic{
      
  public static void main(String args[]){


int numPants = 3;     //defines number of pairs of pants
double pantsPrice = 34.98;    //defines price per pant pair

int numShirts = 2;    // defines number of shirts
double shirtPrice = 24.99; //defines cost per shirt

int numBelts = 1;   //define number of belts
double beltPrice = 33.99;   //defines price per belt

double paSalesTax = 0.06;   //establishes the tax rate
  
double totalCostOfPants = (numPants * pantsPrice);   //total cost of pants
double totalCostOfShirts = (numShirts * shirtPrice);  //total cost of shirts
double totalCostOfBelts = (numBelts * beltPrice);  //total cost of belts
double totalTaxPants = (totalCostOfPants * paSalesTax);     //total cost of tax on pants
double totalTaxShirts = (totalCostOfShirts * paSalesTax);    //total cost of tax on shirts
double totalTaxBelts = (totalCostOfBelts * paSalesTax);     //total cost of tax on belts
double totalCostOfTax = (totalTaxPants + totalTaxShirts + totalTaxBelts);      //total cost of tax of all of them combined        
double totalCostWithTax = (totalCostOfPants + totalCostOfShirts + totalCostOfBelts+totalCostOfTax);    //total cost including shirts pants belts and tax
double totalCostNoTax = (totalCostOfPants + totalCostOfShirts + totalCostOfBelts);      //total cost of clothes without tax
    
        totalCostOfPants = totalCostOfPants * 100;
        totalCostOfPants = (int)(totalCostOfPants);
        totalCostOfPants = totalCostOfPants /100;
      System.out.println("The total cost of pants is " + totalCostOfPants);
    //outputs cost of pants
        totalCostOfShirts = totalCostOfShirts * 100;
        totalCostOfShirts = totalCostOfShirts /100;
    System.out.println("The total cost of shirts is " + totalCostOfShirts);
    //outputs cost of shirts 
        totalCostOfBelts = totalCostOfBelts * 100;
        totalCostOfBelts = totalCostOfBelts /100;
    System.out.println("The total cost of belts is " + totalCostOfBelts);
    //outputs cost of belts
        totalTaxPants = totalTaxPants * 100;
        totalTaxPants = (int)(totalTaxPants);
        totalTaxPants = totalTaxPants /100;
    System.out.println("The total tax on pants is " + totalTaxPants);
    //outputs tax on pants alone
        totalTaxShirts = totalTaxShirts * 100;
        totalTaxShirts = (int)(totalTaxShirts);
        totalTaxShirts = totalTaxShirts /100;
    System.out.println("The total tax on shirts is " + totalTaxShirts);
    //outputs tax on shirts alone
        totalTaxBelts = totalTaxBelts * 100;
        totalTaxBelts = (int)(totalTaxBelts);
        totalTaxBelts = totalTaxBelts /100;
    System.out.println("The total tax on belts is " + totalTaxBelts);
    //outputs tax on belts alone
        totalCostNoTax = totalCostNoTax *100;
        totalCostNoTax = (int)(totalCostNoTax);
        totalCostNoTax = totalCostNoTax /100;
        System.out.println("The total cost of purchase before tax is " + totalCostNoTax);
    //outputs the total cost of the purchase without tax
        totalCostOfTax = totalCostOfTax *100;
        totalCostOfTax = (int)(totalCostOfTax);
        totalCostOfTax = totalCostOfTax /100;
        System.out.println("The total cost of all tax is " + totalCostOfTax);
    //outputs the cost of just the taxes combined 
        totalCostWithTax = totalCostWithTax * 100;
        totalCostWithTax = (int)(totalCostWithTax);
        totalCostWithTax = totalCostWithTax /100;
        System.out.println("The total of the entire transaction is " + totalCostWithTax);
    //outputs total cost of entire transaction including tax
    

      }
  
    }
