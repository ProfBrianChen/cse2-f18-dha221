import java.util.Scanner;

public class Hw07{
   
   Scanner myScanner = new Scanner(System.in);    // Below Scanner
  
     public static void main(String[] args) {
      Scanner myScanner = new Scanner(System.in);
       while(true) {
           //Ask for the string
           System.out.println("Enter a sample text: ");
           String input = myScanner.nextLine();
  
           //Show input
           System.out.println("You entered: " + input);
  
           //Display menu
           printMenu();
           char ch = myScanner.nextLine().charAt(0);
  
           switch (ch) {
           case 'q':
               // close the program
               System.exit(0);
              
           case 'c':   // Number of non-whitespace characters
               int whiteSpace = getNumOfNonWSCharacters(input);
               System.out.println("Number of non-whitespace characters: " + whiteSpace);
               break;
              
           case 'w':   // Number of words
               int numWords = getNumOfWords(input);
               System.out.println("Number of words: " + numWords);
               break;
              
           case 'f':   // Find text
               System.out.println("Enter a word or phrase to be found: ");
               String word = myScanner.nextLine();
               int findWord = findText(input, word);
               System.out.println("\"" + word + "\" instances: " + findWord);
               break;
              
           case 'r':   // Replace all !'s
               String newstring = replaceExclamation(input);
               System.out.println("Edited text: " + newstring);
               break;
              
           case 's':   // Shorten spaces
               String newInput = shortenSpace(input);
               System.out.println("Edited text:" + newInput);
               break;
              
           default: //User invalid entry output
               System.out.println("Invalid option. Please try again");
           }
          
           System.out.println();
       }
   }
   
     private static void printMenu() {
       System.out.println("\nMENU");
       System.out.println("c - Number of non-whitespace characters");
       System.out.println("w - Number of words");
       System.out.println("f - Find text");
       System.out.println("r - Replace all !'s");
       System.out.println("s - Shorten spaces");
       System.out.println("q - Quit");
       System.out.println("\nChoose an option: ");
   }
  
     private static int getNumOfNonWSCharacters(String input) {
       input = input.trim().replaceAll("\\s", "");
       return input.length();
   }
  
     private static int getNumOfWords(String input) {
       input = shortenSpace(input);
       String[] words = input.split(" ");

       return words.length;
   }
  
     private static int findText(String text, String find) {
       int count = 0;
       int i = 0;
      
       while ((i = text.indexOf(find)) != -1) {
           text = text.substring(i + find.length());
           count += 1;
       }

       return count;
     }
       
       private static String replaceExclamation(String text) {
         String replaceExcl = text.replaceAll("!", ".");
         return replaceExcl;
   }

   private static String shortenSpace(String input) {
       String replaceSpace = input.trim().replaceAll(" +", " ");
       return replaceSpace;

   }
  
}