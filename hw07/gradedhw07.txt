
Grading Sheet for HW7

Grade: 97

Compiles    				20/20 pts
Comments 				7/10 pts
PrintMenu Method			10/10 pts
Checks for Invalid Characters	10/10 pts
getNumOfNonWSCharacters 	10/10 pts
getNumOfWords			10/10 pts
findText				10/10 pts
replaceExclamation			10/10 pts
shortenSpace				10/10 pts

Take 5 points off if methods were written but did not work properly

If a student is using Hashmaps or some other Java-based datastructure that is not part of the CSE2 curriculum, take off 10 points and then pretend they are using arrays. (and explain why you took off 10 points)


