
import java.lang.Math;
import java.util.Scanner;

public class CrapsSwitch{
 
  public static void main(String[]args){
    
   Scanner myScanner = new Scanner (System.in);
    
   System.out.println("Would you like your dice to be cast randomly (Type 1 for yes, Type 2 for no) ");
    //asks user if they want randomly rolled dice or to choose their die numbers
   int castType = myScanner.nextInt();      //defines user input "1" as randomly casted and input "2" as they get to choose
    
   switch (castType)
   {
     case 1:      //case one is for if the user selected for their die to be randomly rolled
       int dieNum1 = (int)(Math.random()*6)+1;      //picks a random number 1-6 for die 1
       int dieNum2 = (int)(Math.random()*6)+1;      //picks a random number 1-6 for die 2
       System.out.println("You rolled a " + dieNum1 + " and a " + dieNum2);     //shows user what they rolled randomly
       
       boolean equal = dieNum1 == dieNum2;      //defines a statement equal as sum of die one and two
       
       int randomInt = equal ? 0 : 1;     //randomInt decides whether die one and two values are equal. 0 if true. 1 if false
       int numSum = (dieNum1 + dieNum2);        //defines numSum as sum of die one and two
       
       switch (randomInt){
         case 0:        //case 0 is for if both die are same number
           switch (numSum){
             case 2:      //if both die are a 1 numSum equals 2 
               System.out.println("Snake Eyes");      //prints snake eyes
               break;
             case 4:      //if both die are a 2 numSum equals 4
               System.out.println("Hard Four");       //prints hard four
               break;
             case 6:      //if both die are a 3 numSum equals 6
               System.out.println("Hard Six");      //prints hard six
               break;
             case 8:      //if both die are a 4 numSum equals 8
               System.out.println("Hard Eight");     //prints hard eight
               break;
             case 10:     //if both die are a 5 numSum equals 10
               System.out.println("Hard Ten");        //prints hard ten
               break;
             case 12:     //if both die are a 6 numSum equals 12
               System.out.println("Boxcars");       //prints boxcars
               break;
           }
           
         case 1:        //case 1 within case 0 is if they chose to randomly roll die and if dice are not equal values
           switch (numSum){
             case 3:      //if numSum is 3
               System.out.println("Ace Duece");   //prints ace duece
               break;
             case 4:      //if numSum is 4
               System.out.println("Easy Four");   //prints easy four
               break;
             case 5:      //if numSum is 5
               System.out.println("Fever Five");    //prints fever five
               break;
             case 6:      //if numSum is 6
               System.out.println("Easy Six");      //print easy six
               break;
             case 7:      //if numSum is 7
               System.out.println("Seven Out");     //prints seven out
               break;
             case 8:      //if numSum is 8
               System.out.println("Easy Eight");     //prints easy eight
               break;
             case 9:      //if numSum is 9
               System.out.println("Nine");          //prints nine
                 break;
             case 10:      //if numSum is 10
               System.out.println("Easy Ten");      //prints easy ten
                 break;
             case 11:      //if numSum is 11
               System.out.println("Yo-Leven");      //prints yo-leven
                 break;
              }
           break;
       }
   }
           
           switch (castType){
               
             case 2:            //case two is if the user wants to input their own values of the die
               System.out.println("State the value of the first die ");     //asks user to input first die value
               int dieNum1 = myScanner.nextInt();       //defines next input from user as dieNum1 value
               System.out.println("State the value fo the second die ");    //asks user to input second die value
               int dieNum2 = myScanner.nextInt();       //defines next input from user as dieNum2 value
               
               boolean equal = dieNum1 == dieNum2;        //defines equal as a statement that dieNum1 = dieNum2
       
               int randomInt = equal ? 0 : 1;     //decides if the variable equal is true or false -> if the die are same number or not
               int numSum = dieNum1 + dieNum2;     //defines numSum as the sum of values of die one and two
               
               switch (randomInt){
         case 0:        //case 0 within case 2 is for if the user decides to choose their own and if the values are equal
           switch (numSum){
             case 2:      //numSum equals 2
               System.out.println("Snake Eyes");    //prints snake eyes
               break;
             case 4:      //if numSum equals 4 
               System.out.println("Hard Four");     //prints hard four
               break;
             case 6:      //if numSum equals 6
               System.out.println("Hard Six");      //prints hard six
               break;
             case 8:      //if numSum equals 8
               System.out.println("Hard Eight");    //prints hard eight
               break;
             case 10:     //if numSum equals 10
               System.out.println("Hard Ten");      //prints hard ten
               break;
             case 12:     //if numSum equals 12
               System.out.println("Boxcars");       //prints boxcars
               break;
           }
           break;
         case 1:
           switch (numSum){
             case 3:
               System.out.println("Ace Duece");
               break;
             case 4:
               System.out.println("Easy Four");
               break;
             case 5:
               System.out.println("Fever Five");
               break;
             case 6:
               System.out.println("Easy Six");
               break;
             case 7:
               System.out.println("Seven Out");
               break;
             case 8:
               System.out.println("Easy Eight");
               break;
             case 9:
               System.out.println("Nine");
                 break;
             case 10:
               System.out.println("Easy Ten");
                 break;
             case 11:
               System.out.println("Yo-Leven");
                 break;
              }
           break;
               
           }
           
           
   }
   }
  }