
import java.lang.Math;
import java.util.Scanner;

public class CrapsIf{
 
  public static void main(String[]args){
    
   Scanner myScanner = new Scanner (System.in);
    
   System.out.println("Would you like your dice to be cast randomly (Type 1 for yes, Type 2 for no) ");
   int castType = myScanner.nextInt();            //asks user to input how they'd like their dice to be cast and defines castType as whether they do or don't want it random
   
   if (castType == 1){          //decides if castType is 1 which means user wants randomly rolled die
        int dieNum1 = (int)(Math.random()*6)+1;         //creates random number 1-6 for die one
        int dieNum2 = (int)(Math.random()*6)+1;         //creates random number 1-6 for die two
      
     System.out.println("You rolled a " + dieNum1 + " and a " + dieNum2);       //prints what the user rolled
     if ((dieNum1 == 1) && (dieNum2 == 1)){                 //decides if die one and two are both 1 and outputs snake eyes
       System.out.println("Snake Eyes");
     }
     else if (((dieNum1 == 1) && (dieNum2 == 2)) || ((dieNum1 == 2) && (dieNum2 == 1))){    //decides if roll is a total of 3
     System.out.println("Ace Duece");         //outputs ace duece
     }
     else if ((dieNum1 == 2) && (dieNum2 == 2)){          //decides if both die are a 2
       System.out.println("Hard Four");         //outputs hard four
     }
     else if (((dieNum1 == 1) && (dieNum2 == 3)) || ((dieNum1 == 3) && (dieNum2 == 1))){      //decides if roll is a 4 but not from a 
       System.out.println("Easy Four");         //outputs easy four
     }
     else if (((dieNum1 == 2) && (dieNum2 == 3)) || ((dieNum1 == 3) && (dieNum2 == 2))
       || ((dieNum1 == 1) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2 == 1))){        //decides if roll equals five
       System.out.println("Fever Five");          //outputs fever five
     }
     else if ((dieNum1 == 3) && (dieNum2 == 3)){        //if roll is a 6 from a 3 and 3 
       System.out.println("Hard Six");      //outputs hard six
     }
     else if (((dieNum1 == 2) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2 == 2))
       || ((dieNum1 == 1) && (dieNum2 == 5)) || ((dieNum1 == 5) && (dieNum2 == 1))){      //if roll is a 6 but not from double 3's
       System.out.println("Easy Six");        //outputs easy six
     }
     else if (((dieNum1 == 3) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2 == 3))
       || ((dieNum1 == 2) && (dieNum2 == 5)) || ((dieNum1 == 5) && (dieNum2  == 2))
       || ((dieNum1 == 1) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 1))){     //if roll equals 7
       System.out.println("Seven Out");       //outputs seven out
     }
     else if ((dieNum1 == 4) && (dieNum2 == 4)){        //if both die are a 4
       System.out.println("Hard Eight");        //outputs hard eight
     }
     else if (((dieNum1 == 3) && (dieNum2 == 5)) || ((dieNum1 == 5) && (dieNum2  == 3))
       || ((dieNum1 == 2) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 2))){     //if roll is an 8 but not from double 4's
       System.out.println("Easy Eight");      //outputs easy eight
     }
     else if (((dieNum1 == 5) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2  == 5))
       || ((dieNum1 == 3) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 3))){     //if roll equals a 9
       System.out.println("Nine");      //outputs nine
     }
     else if ((dieNum1 == 5) && (dieNum2 == 5)){        //if roll is double 5's
       System.out.println("Hard Ten");        //outputs hard 10
     }
     else if (((dieNum1 == 6) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2  == 6))){     //if number is 10 but not from double 5's
       System.out.println("Easy Ten");      //outputs easy ten
     }
     else if (((dieNum1 == 5) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 5))){   //if cumulitive roll is an 11
       System.out.println("Yo-Leven");      //outputs yo-leven
     }
     else if ((dieNum1 == 6) && (dieNum2 == 6)){      //if roll is a 12
       System.out.println("Boxcars");       //outputs boxcars
     }
   }
        else if (castType == 2){          //if castType is 2 then user wants to choose their roll
     System.out.println("State the value of the first die ");     //asks user to input first die value
     int dieNum1 = myScanner.nextInt();       //defines dieNum1 as the next int the user inputs
     System.out.println("State the value of the second die ");     //asks user ot input second die value
     int dieNum2 = myScanner.nextInt();       //defines dieNum2 as next int the user inputs
     
     if ((dieNum1 <= 6 && dieNum1 >= 1) && (dieNum2 <= 6 && dieNum2 >= 1)){         //checks to make sure user didnt input any invalid die numbers
      if ((dieNum1 == 1) && (dieNum2 == 1)){          //checks if the roll is double 1's
       System.out.println("Snake Eyes");        //outputs snakeyes 
     }
     else if (((dieNum1 == 1) && (dieNum2 == 2)) || ((dieNum1 == 2) && (dieNum2 == 1))){      //checks if roll is a 3
     System.out.println("Ace Duece");       //outputs ace duece
     }
     else if ((dieNum1 == 2) && (dieNum2 == 2)){      //checks if roll is double 2's
       System.out.println("Hard Four");           //outputs hard four
     }
     else if (((dieNum1 == 1) && (dieNum2 == 3)) || ((dieNum1 == 3) && (dieNum2 == 1))){    //checks if roll is 4 but not double 2's
       System.out.println("Easy Four");       //outputs easy four
     }
     else if (((dieNum1 == 2) && (dieNum2 == 3)) || ((dieNum1 == 3) && (dieNum2 == 2))
       || ((dieNum1 == 1) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2 == 1))){    //checks if roll is a 5
       System.out.println("Fever Five");      //outputs fever five
     }
     else if ((dieNum1 == 3) && (dieNum2 == 3)){    //checks if roll is double 3's
       System.out.println("Hard Six");      //outputs hard six
     }
     else if (((dieNum1 == 2) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2 == 2))
       || ((dieNum1 == 1) && (dieNum2 == 5)) || ((dieNum1 == 5) && (dieNum2 == 1))){    //checks if roll is a 6 but not from double 3's
       System.out.println("Easy Six");        //outputs easy six
     }
     else if (((dieNum1 == 3) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2 == 3))
       || ((dieNum1 == 2) && (dieNum2 == 5)) || ((dieNum1 == 5) && (dieNum2  == 2))
       || ((dieNum1 == 1) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 1))){     //checks if roll is a 7
       System.out.println("Seven Out");     //outputs seven out
     }
     else if ((dieNum1 == 4) && (dieNum2 == 4)){    //checks if roll is double 4's
       System.out.println("Hard Eight");      //outputs hard eight
     }
     else if (((dieNum1 == 3) && (dieNum2 == 5)) || ((dieNum1 == 5) && (dieNum2  == 3))
       || ((dieNum1 == 2) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 2))){   //checks if roll is 8 but not double 4's
       System.out.println("Easy Eight");      //outputs hard eight
     }
     else if (((dieNum1 == 5) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2  == 5))
       || ((dieNum1 == 3) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 3))){     //checks if roll is a 9
       System.out.println("Nine");      //outputs nine
     }
     else if ((dieNum1 == 5) && (dieNum2 == 5)){        //checks if roll is double 5's
       System.out.println("Hard Ten");      //outputs hard ten
     }
     else if (((dieNum1 == 6) && (dieNum2 == 4)) || ((dieNum1 == 4) && (dieNum2  == 6))){   //checks if roll is 10 but not double 5's
       System.out.println("Easy Ten");      //outputs easy 10
     }
     else if (((dieNum1 == 5) && (dieNum2 == 6)) || ((dieNum1 == 6) && (dieNum2  == 5))){   //checks if roll is an 11
       System.out.println("Yo-Leven");      //outputs yo-leven
     }
     else if ((dieNum1 == 6) && (dieNum2 == 6)){    //checks if roll is double 6's
       System.out.println("Boxcars");     //outputs boxcars
     }
     }
     }
 
  }
}
