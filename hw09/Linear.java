
import java.util.Scanner;
import java.util.Random;

public class Linear{
  
 public static void main(String[] args) {
   Scanner myScanner = new Scanner(System.in);
   
  int numGrades = 15;
  int[] grades = new int[numGrades];
  int input;
  
  System.out.println("Enter " + numGrades + " integers in ascending order as final grades in CSE2: ");

  for (int i = 0; i < grades.length; ++i){

    if (!myScanner.hasNextInt()){                    //checks if value is int
    System.out.println("Error: Value entered was not an integer");
    --i;
      continue;
   }
    
   grades[i] = myScanner.nextInt();              
   
   if (grades[i] > 100 || grades[i] < 0){            //checks if value is between 0 and 100
    System.out.println("Error: Integer must be between 0-100");
    --i;
     continue;
   }
   

     //checks if integers being entered are in ascending order
    if (grades[i] < grades[i - 1]){
     System.out.println("Error: Integers must be in ascending order");
      --i;
      continue;
   }
  }
   
  for (int i = 0; i < grades.length; i++) {               //prints out array
   System.out.print(grades[i] + " ");
  }
   
  System.out.println();
   
  System.out.println("Enter a grade to search for: ");
   
  input = myScanner.nextInt();
  
  binarySearch(grades, input);
   
  int[] scrambled = arrayScramble(grades);           //prints out the scrambled array
  
  System.out.println("Scrambled: ");
   
 for(int i = 0; i < scrambled.length; i++) {
  System.out.print(scrambled[i] + " ");
 }
 System.out.println();
 //asks user to enter another integer to search for using linear search
 System.out.print("Enter a grade to search for: ");
 input = myScanner.nextInt();
  linearSearch(scrambled, input);
 }

 public static void linearSearch(int[] grades, int input) {
  int i;

  for (i = 0; i < grades.length; i++) {

    if (grades[i] == input) {
    System.out.println("During linear search the number " + input + " was found after " + i + " comparisons");
    break;
      
   }
    
   
   if (i == grades.length - 1) {                  //prints if number not found
    System.out.println("During linear search the number " + input + " was not found after " + i + " comparisons");
   }
  }
  System.out.println("");
 }
  

 public static void binarySearch(int[] grades, int input) {
   
  int low = 0;
  int high = grades.length;
  int middle;
  int counter = 0;
  boolean tf;
 
  while(low < high){
    
    middle = (high + low)/2;
    ++counter;
    
    if(grades[middle] == input){
      tf = true;
      break;
    }
    
    if(grades[middle] < input){
      low = middle + 1;
    }
    
    if(grades[middle] > input){
      high = middle - 1;
    }
  }
  
  if(tf == true){                 //prints the number and how many cuts were used to find
    System.out.println("Binary Search: " + input + " was found in the list with " + counter + " iterations");
  }
   
   else{                          //prints that number doesnt occur if not found
     System.out.println("Binary Search: Grade does not exist");
  }
 }

 public static int[] arrayScramble(int[] grades) {
   
  int[] scrambled = new int[grades.length];
  Random random = new Random();
  

  for (int i = 0; i < grades.length; i++) {
   scrambled[i] = grades[i];
  }
  

  for (int j = 0; j < scrambled.length; j++) {
    
   int randomIndex = random.nextInt(scrambled.length);
   int temp = scrambled[j];
   scrambled[j] = scrambled[randomIndex];
   scrambled[randomIndex] = temp;
    
  }

   
  return grades; 
 }

}