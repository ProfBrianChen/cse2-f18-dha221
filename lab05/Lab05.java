
import java.lang.Math;
import java.util.Scanner;

public class Lab05{
  
    public static void main(String args[]){
      
      Scanner myScanner = new Scanner (System.in);
      
      ///course number, department name,
      //the number of times it meets in a week, the time the class starts,
      //the instructor name, and the number of students
      
      int courseNumber;     //course number
      String depName;       //department name
      int numMeet;          //number times meets per week
      double timeStart;        //time class starts
      String profName;      //professor name
      int studentNum;       //number of students in class
      
      ////COURSE NUMBER
      System.out.println("Enter the course number: ");
      while (! myScanner.hasNextInt()){
        
        System.out.println("Error, enter a valid course number: ");
        myScanner.next();
        
      }
      
      courseNumber = myScanner.nextInt();
      
      ///DEPARTMENT NAME
      System.out.println("Enter the department name: ");
      myScanner.next();
        
      while (! myScanner.hasNextLine()){
        
        System.out.println("Error, enter valid department name: ");
        myScanner.next();
        
      }
      
      depName = myScanner.nextLine();
      
      ///MEET PER WEEK
      System.out.println("Enter the number of times you meet a week: ");
      while (! myScanner.hasNextInt()){
        
        System.out.println("Error, enter valid number of times per week: ");
        myScanner.next();
        
      }
      
      numMeet = myScanner.nextInt();
      
      ///TIME START
      System.out.println("Enter the time that the class starts: ");
      while (!myScanner.hasNextInt()){
        
        System.out.println("Error, enter valid time: ");
        myScanner.next();
        
      }
      
      timeStart = myScanner.nextInt();
      
      
      ///PROFESSOR NAME
      System.out.println("Enter the name of professor: ");
      myScanner.next();
      
      while (! myScanner.hasNextLine()){
        
        System.out.println("Error, enter valid name: ");
        myScanner.next();
        
      }
      
      profName = myScanner.nextLine();
      
      ///NUMBER STUDENTS
      System.out.println("Enter the number of students in course: ");
      while (! myScanner.hasNextInt()){
        
        System.out.println("Error, enter valid number of students: ");
        myScanner.next();
        
      }
      
      studentNum = myScanner.nextInt();
      
      
      System.out.println("Course Number: " + courseNumber);
      System.out.println("Department Name: " + depName);
      System.out.println("Meetings Per Week: " + numMeet);
      System.out.println("Class Start Time: " + timeStart);
      System.out.println("Name of Professor: " + profName);
      System.out.println("Number of Students: " + studentNum);
      
    }
}