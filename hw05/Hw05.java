
import java.lang.Math;
import java.util.Scanner;

public class Hw05{

    public static void main(String args[]) {

        Scanner myScanner = new Scanner(System.in);

        System.out.println("Enter how many hands you want to be dealt: ");      ///asks user to input number of dealt hands
        int dealNum = myScanner.nextInt();        ///takes next int as deal number

        int counter = 1;      //defines counter
        int onePair = 0;      //variable for number of one pair
        int twoPair = 0;      //variable for number of two pairs
        int threeKind = 0;    //variable for three of a kinds
        int fourKind = 0;     //variable for four of a kinds

        int card1 = -1;         /////defines all card numbers
        int card2 = -1;
        int card3 = -1;
        int card4 = -1;
        int card5 = -1;

        while (counter <= dealNum) {

            card1 = (int) (Math.random() * 52) + 1;         ///gives every card a random value between 1-52
            card2 = (int) (Math.random() * 52) + 1;
            card3 = (int) (Math.random() * 52) + 1;
            card4 = (int) (Math.random() * 52) + 1;
            card5 = (int) (Math.random() * 52) + 1;

            while (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5) {
                card1 = (int) (Math.random() * 52) + 1;
            }
            while (card2 == card3 || card2 == card4 || card2 == card5) {
                card2 = (int) (Math.random() * 52) + 1;
            }
            while (card3 == card4 || card3 == card5) {
                card3 = (int) (Math.random() * 52) + 1;
            }
            while (card4 == card5) {
                card4 = (int) (Math.random() * 52) + 1;         ////all combinations of different cards equaling each other
            }
          

            card1 %= 13;            /////assigns card value to cards (1-13)
            card2 %= 13;
            card3 %= 13;
            card4 %= 13;
            card5 %= 13;

            int pair = 0;

            if (card1 == card2 && card1 == card3 && card1 == card4
                    || card1 == card2 && card1 == card3 && card1 == card5
                    || card1 == card2 && card1 == card4 && card1 == card5
                    || card1 == card3 && card1 == card4 && card1 == card5
                    || card2 == card3 && card2 == card5 && card2 == card4) {
                fourKind++;                                                       ///if four card have equal value of 1-13 
            } else if (card1 == card2 && card1 == card3                           ///then four of a kind variable is incrememented
                    || card1 == card2 && card1 == card4                           ///for each different hand
                    || card1 == card2 && card1 == card5
                    || card1 == card3 && card1 == card4
                    || card1 == card3 && card1 == card5
                    || card1 == card4 && card1 == card5
                    || card2 == card3 && card2 == card4
                    || card2 == card3 && card2 == card5
                    || card2 == card4 && card2 == card5
                    || card3 == card4 && card3 == card5) {              //if any three cards are all equal in a hand
                threeKind++;                                             //three of kind var is incrememnted
            } else {
                if (card1 == card2) {
                    pair++;
                }
                if (card1 == card3) {
                    pair++;
                }
                if (card1 == card4) {
                    pair++;
                }
                if (card1 == card5) {
                    pair++;
                }
                if (card2 == card3) {
                    pair++;
                }
                if (card2 == card4) {
                    pair++;
                }
                if (card2 == card5) {
                    pair++;
                }
                if (card3 == card4) {
                    pair++;
                }
                if (card3 == card5) {
                    pair++;
                }
                if (card4 == card5) {                         //increment if any 2 cards are equal in a hand
                    pair++;
                }
            }

            if (pair == 1) {
                onePair++;
            } else if (pair == 2) {
                twoPair++;
            }
            if (pair == 1) {
                onePair++;
            }

            counter++;
        }
        double probPair = (double) onePair / dealNum;             //probbility of getting one pair in the hands
        double prob2Pair = (double) twoPair / dealNum;            //probability of getting two pair 
        double prob3Kind = (double) threeKind / dealNum;          //prob of getting three of a kind
        double prob4Kind = (double) fourKind / dealNum;           //prob of getting four of a kind 

        System.out.println("The number of hands is: " + dealNum);               //outputs # of hands dealt
        System.out.println("The probability of a Pair is: " + probPair);        //outputs probability 
        System.out.println("The probability of a Two Pair is: " + prob2Pair);
        System.out.println("The probability of Three of a Kind is: " + prob3Kind);
        System.out.println("The probability of Four of a Kind is: " + prob4Kind);

    }
}